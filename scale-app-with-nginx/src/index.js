const express = require('express');

const app = express();

const PORT = process.env.PORT || 3000;
const APP_NUM = process.env.APP_NUM || 1;

app.get('/', function(req, res) {
  res.json({ server: APP_NUM });
});

app.listen(PORT, () => console.log(`Server is start on ${PORT}`));
