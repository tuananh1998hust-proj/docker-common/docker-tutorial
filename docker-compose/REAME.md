# Docker compose example

```
// Running docker-compose
$ docker-compose up
// Access to container
$ docker exec -it mongo mongo
// See database
> show dbs show
admin    0.000GB
config   0.000GB
local    0.000GB
// Switch database test
> use test
switched to db test
> db.user.insert({"name": "docker"})
WriteResult({ "nInserted" : 1 })
> show collections
user
```
