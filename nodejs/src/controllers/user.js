const User = require('../models/user');

async function GetUser(req, res) {
  try {
    const users = await User.find();

    res.json({ users });
  } catch (error) {
    console.log(`GetUser error ${error}`);

    res.json({ error: 'Bad Request' });
  }
}

async function CreateUser(req, res) {
  try {
    const { name, age } = req.body;

    const user = new User({
      name,
      age
    });

    await user.save();

    res.json({ user });
  } catch (error) {
    console.log(`CreateUser error ${error}`);

    res.json({ error: 'Bad Request' });
  }
}

async function UpdateUser(req, res) {
  try {
    const { name, age } = req.body;
    const { id } = req.params;

    const user = await User.findById(id);

    user.name = name;
    user.age = age;

    await user.save();

    res.json({ user });
  } catch (error) {
    console.log(`UpdateUser error ${error}`);

    res.json({ error: 'Bad Request' });
  }
}

async function DeleteUser(req, res) {
  try {
    const { id } = req.params;

    const user = await User.findById(id);

    await user.delete();

    res.json({ delete: 'Ok' });
  } catch (error) {
    console.log(`DeleteUser error ${error}`);

    res.json({ error: 'Bad Request' });
  }
}

module.exports = {
  GetUser,
  CreateUser,
  UpdateUser,
  DeleteUser
};
