const express = require('express');
const mongoose = require('mongoose');

const config = require('../config/main');
const UserRoutes = require('./routes/user');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// RetryConnection :
function RetryConnection() {
  console.log('MongoDB connect with retry');

  mongoose.connect(config.MONGO_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
}

mongoose.connection.on('error', err => {
  console.log(`Database connection error: ${err}`);

  setTimeout(RetryConnection, 5000);
});

mongoose.connection.on('connected', () => {
  console.log('Database connected!');
});

mongoose.connect(config.MONGO_URI, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
});

// Use Routes
app.use('/api/v1/user', UserRoutes);

app.listen(config.PORT, () => {
  console.log(`Server is running on port ${config.PORT}`);
});
