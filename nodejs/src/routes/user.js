const express = require('express');
const UserControllers = require('../controllers/user');

const router = express.Router();

router.get('/', UserControllers.GetUser);
router.post('/', UserControllers.CreateUser);
router.put('/:id', UserControllers.UpdateUser);
router.delete('/:id', UserControllers.DeleteUser);

module.exports = router;
