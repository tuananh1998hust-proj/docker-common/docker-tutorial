const key = {
  NODE_ENV: process.env.NODE_ENV || 'development',
  MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost:27017/nodejs',
  PORT: process.env.PORT || 5000
};

module.exports = key;
